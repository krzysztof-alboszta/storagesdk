<?php

namespace Skladiste\SDKBundle\StorageSDK\API;

use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\Strategy\MockClientStrategy;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Skladiste\SDKBundle\StorageSDK\Client;

class ProductTest extends TestCase
{

    protected function setUp()
    {
        parent::setUp();
        HttpClientDiscovery::prependStrategy(MockClientStrategy::class);
    }

    public function testAvailable(): void
    {
        $expected = [
            ['id' => 12, 'name' => 'Lorem ipsum', 'amount' => 5],
            ['id' => 21, 'name' => 'Lorem ipsum', 'amount' => 1],
        ];

        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('get')
            ->with('/products?collection=available', [])
            ->will($this->returnValue($expected));

        self::assertEquals($expected, $api->available());
    }

    // TODO: handle not found
    public function testShow(): void
    {
        $expected = ['id' => 12, 'name' => 'Lorem ipsum', 'amount' => 5];
        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('get')
            ->with('/products/12', [])
            ->will($this->returnValue($expected));

        self::assertEquals($expected, $api->show(12));
    }

    public function testAll(): void
    {
        $expected = [
            ['id' => 12, 'name' => 'Lorem ipsum', 'amount' => 5],
            ['id' => 21, 'name' => 'Dolor sit amet', 'amount' => 0],
            ['id' => 122, 'name' => 'consectetur adipiscing elit', 'amount' => 1],
        ];

        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('get')
            ->with('/products', [])
            ->will($this->returnValue($expected));

        self::assertEquals($expected, $api->all());
    }

    public function testSafeAvailable(): void
    {
        $expected = [
            ['id' => 12, 'name' => 'Lorem ipsum', 'amount' => 5],
        ];

        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('get')
            ->with('/products?collection=safe-available', [])
            ->will($this->returnValue($expected));

        self::assertEquals($expected, $api->safeAvailable());
    }

    public function testUpdate(): void
    {
        $api = $this->getApiMock();
        $api->expects($this->once())
            ->method('put')
            ->with('/products/21', ['amount' => 12]);

        $api->update(21, ['amount' => 12]);
    }

    /** @return Product|MockObject */
    protected function getApiMock(): MockObject
    {
        $client = new Client('API-KEY', 'http://localhost/api');

        return $this->getMockBuilder(Product::class)
            ->setMethods(['get', 'post', 'postRaw', 'patch', 'delete', 'put', 'head'])
            ->setConstructorArgs([$client])
            ->getMock();
    }

}
