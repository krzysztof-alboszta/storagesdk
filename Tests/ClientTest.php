<?php

namespace Skladiste\SDKBundle\StorageSDK;


use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\Strategy\MockClientStrategy;
use PHPUnit\Framework\TestCase;
use Skladiste\SDKBundle\StorageSDK\API\Product;

class ClientTest extends TestCase
{
    protected function setUp()
    {
        parent::setUp();
        HttpClientDiscovery::prependStrategy(MockClientStrategy::class);
    }

    public function test_getHttpClient(): void
    {
        $client = new Client('API-KEY', 'http://localhost/api');
        $this->assertInstanceOf(\Http\Client\HttpClient::class, $client->getHttpClient());
    }

    public function test_getApiInstance(): void
    {
        $client = new Client('API-KEY', 'http://localhost/api');

        $api = $client->api('products');

        self::assertInstanceOf(Product::class, $api);
    }

    public function test_throwsException_onUnknownApi(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $client = new Client('API-KEY', 'http://localhost/api');
        $client->api('unknown_api');
    }
}
