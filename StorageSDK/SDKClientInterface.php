<?php

namespace Skladiste\SDKBundle\StorageSDK;

use Http\Client\Common\HttpMethodsClient;
use Skladiste\SDKBundle\StorageSDK\API\StorageApi;

interface SDKClientInterface
{
    /** returns API dedicated for given resource name */
    public function api(string $name): StorageApi;

    /** returns HTTP client implementation */
    public function getHttpClient(): HttpMethodsClient;
}