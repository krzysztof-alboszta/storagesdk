<?php

namespace Skladiste\SDKBundle\StorageSDK\DTO;

class Product
{
    private $id;
    private $name;
    private $amount;

    public function __construct(int $id, string $name, int $amount)
    {
        $this->id = $id;
        $this->name = $name;
        $this->amount = $amount;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}