<?php

namespace Skladiste\SDKBundle\StorageSDK\API;


use Psr\Http\Message\ResponseInterface;
use Skladiste\SDKBundle\StorageSDK\Client;

class AbstractApi implements StorageApi
{

    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /** @throws \Http\Client\Exception */
    protected function get(string $path, array $parameters)
    {
        if (\count($parameters) > 0) {
            $path .= '?' . http_build_query($parameters);
        }

        $response = $this->client->getHttpClient()->get($path);

        $body = $response->getBody()->__toString();
        $content = \json_decode($body, true);

        if (JSON_ERROR_NONE !== \json_last_error()) {
            return $body;
        }

        return $content;
    }

    /** @throws \Http\Client\Exception */
    protected function post(string $path, array $parameters): ResponseInterface
    {
        return $this->client->getHttpClient()->post($path, [], \json_encode($parameters));
    }

    /** @throws \Http\Client\Exception */
    protected function put(string $path, array $parameters): ResponseInterface
    {
        return $this->client->getHttpClient()->put($path, [], \json_encode($parameters));
    }

    /** @throws \Http\Client\Exception */
    protected function delete(string $path, array $parameters): ResponseInterface
    {
        return $this->client->getHttpClient()->delete($path, [], \json_encode($parameters));
    }
}