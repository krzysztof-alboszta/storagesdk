<?php

namespace Skladiste\SDKBundle\StorageSDK\API;

use Psr\Http\Message\ResponseInterface;

class Product extends AbstractApi
{

    public function all()
    {
        return $this->get('/products.json', []);
    }

    public function available(): array
    {
        return $this->get('/products.json?collection=available', []);
    }

    public function unavailable(): array
    {
        return $this->get('/products.json?collection=unavailable', []);
    }

    public function safeAvailable(): array
    {
        return $this->get('/products.json?collection=safe-available', []);
    }

    /** @throws ProductNotFound */
    public function show(int $id): array
    {
        return $this->get('/products.json/' . $id, []);
    }

    public function create(array $data): ResponseInterface
    {
        return $this->post('/products.json', $data);
    }

    public function update(int $id, array $data): ResponseInterface
    {
        return $this->put('/products.json/' . $id, $data);
    }
}