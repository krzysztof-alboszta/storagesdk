<?php

namespace Skladiste\SDKBundle\StorageSDK;

use Http\Client\Common\HttpMethodsClient;
use Http\Client\Common\Plugin\BaseUriPlugin;
use Http\Client\Common\Plugin\HeaderDefaultsPlugin;
use Http\Client\Common\PluginClient;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;
use Http\Discovery\UriFactoryDiscovery;
use Skladiste\SDKBundle\StorageSDK\API\Product;
use Skladiste\SDKBundle\StorageSDK\API\StorageApi;

final class Client implements SDKClientInterface
{
    private const HEADER_X_AUTH_TOKEN = 'X-AUTH-TOKEN';
    private $baseUri;
    private $apiKey;

    public function __construct(string $apiKey, string $baseUri)
    {
        $this->apiKey = $apiKey;
        $this->baseUri = $baseUri;
    }

    /** @throws \InvalidArgumentException */
    public function api(string $name): StorageApi
    {
        switch ($name) {
            case
            'products':
                return new Product($this);
            default:
                throw new \InvalidArgumentException('Invalid API');
        }
    }

    public function getHttpClient(): HttpMethodsClient
    {
        $baseUriPlugin = new BaseUriPlugin(
            UriFactoryDiscovery::find()->createUri($this->baseUri),
            [
                'replace' => true,
            ]
        );

        $headerDefaultsPlugin = new HeaderDefaultsPlugin([
            self::HEADER_X_AUTH_TOKEN => $this->apiKey
        ]);

        $pluginClient = new PluginClient(
            HttpClientDiscovery::find(),
            [$baseUriPlugin, $headerDefaultsPlugin]
        );

        return new HttpMethodsClient(
            $pluginClient,
            MessageFactoryDiscovery::find()
        );
    }
}