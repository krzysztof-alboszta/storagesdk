Skladiste SDK Bundle
=======================

Bundle that integrate API Client for Storage API

Installation
============


```console
$ composer require skladiste/skladiste-sdk-bundle php-http/guzzle6-adapter
```

Usage
======

Client:

```php
$apiClient = $container->get('skladiste_sdk.client`);

$api = $apiClient->api('products');

$allProducts = $api->all();
$availableProducts = $api->available();
$nonAvailableProducts = $api->unavailable();

$moreThan5InStock = $api->safeAvailable();

$product = $api->show($productId);

$api->update($productId, ['amount' => 5]);

$api->create(['name' => 'Super product', 'amount' => 100]);
```